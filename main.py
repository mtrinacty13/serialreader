import sys

import serial
import time
import logging
import datetime


def setup_logfile(test):
    date = datetime.datetime.now()
    file = 'DWM_posLogFile_'+test+'_'+str(date.today())+'_'+str(date.time())
    file = file.replace(':', '_')
    file = file.replace('.', '')
    file += '.log'
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)  # or whatever
    handler = logging.FileHandler(file, 'w', 'utf-8')  # or whatever
    #handler.setFormatter(logging.Formatter('%(name)s %(message)s'))  # or whatever
    root_logger.addHandler(handler)


if __name__ == '__main__':
    print(sys.argv)
    setup_logfile(sys.argv[2])
    print('DWM')
    #script argument define port just a number
    DWM = serial.Serial(port="/dev/ttyACM"+sys.argv[1], baudrate=115200)
    print("Connected to " + DWM.name)
    DWM.write("\r\r".encode())
    time.sleep(1)
    DWM.write("si\r".encode())
    mode = DWM.readline().decode()
    print("mode")
    print(mode)
    time.sleep(1)
    DWM.write("les\r".encode())
    time.sleep(1)
    while True:
        try:
            line = DWM.readline()
            logging.info(line)
            print(line.decode())

        except Exception as ex:
            print(ex)
            break
    DWM.write("\r".encode())
    DWM.close()
