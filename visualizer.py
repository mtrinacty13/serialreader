import sys

import numpy as np
import imageio
from matplotlib import pyplot as plt
import scipy.ndimage as ndimage

from matplotlib import animation


def openfile():
    # f = open("DWM_posLogFile_2021-07-08 13_37_00867519_13_37_00867510.log", "r")
    f = open("DWM_posLogFile_trailerTestMeasured_1_2021-07-14 14_49_15216908_14_49_15216892.log", "r")

    output = False

    values = np.empty((0, 4), dtype=float)
    got_value = False

    for x in f:
        if "les" in x:
            output = True
            continue
        if output:
            if "loc_data" in x:  # and len(actual)<1:
                timevalue = x[x.find('[') + 1:13]
            if not "loc_data" in x and x[3].isdigit():
                datavalue = x[x.find('[') + 1:x.find(']')].split(',')
                datavalue = np.array(datavalue[0:3])
                got_value = True
            if got_value:
                got_value = False
                data = np.concatenate([[timevalue], datavalue])
                values = np.vstack([values, data])

    # print(values)
    return values.astype(float)

def openfiletag(tagLogFile):
    f = open(tagLogFile, "r")
    lines = f.readlines()
    output = False
    anchors = []
    anchorsStr = []
    anchorsConnect = []
    actualAnchors = []
    got_pos = False
    posTag = [0,0]
    listVal = []
    firstPos=[]

    for pos in lines:
        if "est" in pos and not firstPos:
            firstPos = pos[pos.find('est[') + 4:pos.find(']' + '\\' + 'r')].split(",")
            posTag = [firstPos[0], firstPos[1]]
            break

    for x in lines:
        x = x.replace("b'", "").replace("dwm>", "")
        if "dwm>" in x:
            #print("Invalid literal"+x)
            continue
        if output:
            if "est" in x:  # and len(actual)<1:
                datavalue = x[x.find('est[') + 4:x.find(']' + '\\' + 'r')].split(",")
                listVal.append(list(map(float, datavalue[0:3])))

            #print(x.split(' '))
            for anchor in x.split(' '):
                print(x.split(' '))
                if anchor and not anchor.startswith('le') and not anchor.startswith('est') and not anchor.startswith('\\'):

                    if int(anchor[0:4], 16) :
                        pos = anchor[anchor.find('[') + 1:anchor.find(']')].split(",")
                        actualAnchors.append([float(pos[0]), float(pos[1])])

                        if anchor[0:4] not in anchorsStr and len(anchors) <= 30:
                            anchorsStr.append(anchor[0:4])
                            anchorPos = anchor[anchor.find('[') + 1: anchor.find(']')].split(',')
                            anchors.append([anchor[0:4], list(map(float, anchorPos[0:3]))])
                if anchor.startswith('est'):
                    posTag = anchor[anchor.find('[') + 1:anchor.find(']')].split(",")
                    actualAnchors.append([float(posTag[0]), float(posTag[1])])
                    got_pos=True
            if not got_pos:
                actualAnchors.append([float(posTag[0]), float(posTag[1])])
            got_pos=False

            anchorsConnect.append(actualAnchors)
            actualAnchors = []
        if "les" in x:
            output = True

    return listVal, anchors,anchorsConnect
def plotTag(data,anchors):
    anchorX = []
    anchorY = []
    x=[]
    y=[]

    for anchor in anchors:
        anchorX.append(anchor[1][0])
        anchorY.append(anchor[1][1])

    for coordinates in data:
        x.append(coordinates[0])
        y.append(coordinates[1])
    plt.plot(x, y, "o-")
    plt.plot(anchorX,anchorY,"^")
    plt.xlim([-10, 40])
    plt.ylim([-10, 40])
    plt.show()


def checkAnchorHex(s):
    for ch in s:
        if (ch < '0' or ch > '9') and (ch < 'A' or ch > 'F'):
            return False
    print("true")
    return True

def plot(data, xlim, ylim):
    x = data[:, 1]
    y = data[:, 2]
    # print(x)
    # print(y)

    plt.plot(x, y, "o-")
    plt.xlim([-50, 50])
    plt.ylim([-50, 50])
    plt.show()


# anim variables
fig, ax = plt.subplots()
data, anchors, lineToAnchor = openfiletag(sys.argv[1])
xAnim = []
yAnim = []
anchorX = []
anchorY = []
img = imageio.imread("floorPlan.png")
rotatedImg= ndimage.rotate(img,90)
def init():
    global fig, ax, data, xAnim, yAnim
    fig, ax = plt.subplots()
    data = openfiletag(sys.argv[1])
    xAnim = []
    yAnim = []

def animate(i):
    #xAnim.append(data[i][0])
    #yAnim.append(data[i][1])
    #print(lineToAnchor[i][0])

    #print(lineToAnchor[i][1])
    ax.clear()
    ax.plot(anchorX, anchorY, "^")
    ax.imshow(rotatedImg, zorder=0, extent=[-35, 48, -5.5, 28])
    idx = 0
    lenOfLine= len(lineToAnchor[i])
    for vals in lineToAnchor[i]:
        if idx == lenOfLine-1:
            break
        xValues = [lineToAnchor[i][-1][0], vals[0]]
        yValues = [lineToAnchor[i][-1][1],vals[1]]
        ax.plot(xValues,yValues, ':r')
        idx+=1
    xAnim.append(lineToAnchor[i][-1][0])
    yAnim.append(lineToAnchor[i][-1][1])
    ax.plot(xAnim, yAnim)
    #print("line"+str(lineToAnchor[i]))
    #print(len(lineToAnchor[i]))
    # if(len(lineToAnchor[i])<1):
    #     for lines in range(4):
    #         xValues = [data[i][0], data[i][0]]
    #         yValues = [data[i][1], data[i][1]]
    #         ax.plot(xValues, yValues, ':r')
    # else:
    #     for lines in lineToAnchor[i]:
    #         xValues=[data[i][0],lines[0]]
    #         yValues=[data[i][1],lines[1]]
    #         ax.plot(xValues,yValues, ':r')
    #
    # print("tag:["+str(data[i][0])+","+str(data[i][1])+"]")
    #
    # if(len(lineToAnchor[i])<1):
    #     for lines in range(4):
    #         print("anchor:["  + str(data[i][0]) + "," + str(data[i][1]) + "]")
    # else:
    #     for lines in lineToAnchor[i]:
    #         print("anchor:[" + str(lines[0]) + "," + str( lines[1]) + "]")
    #
    ax.set_xlim([-10,40])
    ax.set_ylim([-10,16])


if __name__ == "__main__":
    print(sys.argv[1])
    for anchor in anchors:
        anchorX.append(anchor[1][0])
        anchorY.append(anchor[1][1])
    anim = animation.FuncAnimation(fig, animate, frames=len(lineToAnchor), interval=1, repeat=False)

    anim.save(sys.argv[1].split(" ")+"_video.mp4", fps=11)

    #plt.show()